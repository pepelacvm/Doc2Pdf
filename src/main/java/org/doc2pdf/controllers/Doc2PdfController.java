package org.doc2pdf.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.doc2pdf.converters.Doc2PdfConverter;
import org.doc2pdf.helpers.Doc2PdfHelper;
import org.doc2pdf.model.JsonFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class Doc2PdfController {
	
	@Value("${tmp.dir}")
	private String tmpDir;
	
	@Autowired
	private Doc2PdfHelper helper;

	@PostMapping("/doc2pdf")
	public @ResponseBody String content(@RequestBody String payload) throws FileNotFoundException, IOException {
	    System.out.println(payload);
	    ObjectMapper mapper = new ObjectMapper();
	    JsonFile jsonFile = mapper.readValue(payload, JsonFile.class);
	    //сохраняем файл во временную директорию
	    String docxFilePath = helper.save(jsonFile);
	    //конвертируем
        Doc2PdfConverter converter = new Doc2PdfConverter();
        String pdfFilePath = converter.convertDocx2pdf(docxFilePath);
        //загружаем и возвращаем pdf файл
		return helper.load(pdfFilePath);
	}
}
