package org.doc2pdf.converters;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class Doc2PdfConverter {
	Logger logger = LoggerFactory.getLogger(Doc2PdfConverter.class);
	

	public String convertDocx2pdf(String docxFilePath) {

		File docxFile = new File(docxFilePath);
		String pdfFilePath = docxFilePath.substring(0, docxFilePath.lastIndexOf(".docx")) + ".pdf";

		if (docxFile.exists()) {
		    if (!docxFile.isDirectory()) { 
		        ActiveXComponent app = null;
		        long start = System.currentTimeMillis();
		        try {
		            app = new ActiveXComponent("Word.Application");
		            Dispatch documents = app.getProperty("Documents").toDispatch();
		            Dispatch document = Dispatch.call(documents, "Open", docxFilePath, false, true).toDispatch();
		            File target = new File(pdfFilePath);
		            if (target.exists()) {
		                target.delete();
		            }
		            Dispatch.call(document, "SaveAs", pdfFilePath, 17);
		            Dispatch.call(document, "Close", false);
		            long end = System.currentTimeMillis();
		            logger.info("============Convert Finished：" + (end - start) + "ms");
		        } catch (Exception e) {
		            logger.error(e.getLocalizedMessage(), e);
		            throw new IllegalStateException("pdf convert failed.");
		        } finally {
		            if (app != null) {
		                app.invoke("Quit", new Variant[] {});
		            }
		        }
		    }
		}
		return pdfFilePath;
	}
}
