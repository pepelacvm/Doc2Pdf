package org.doc2pdf.helpers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import org.doc2pdf.model.JsonFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Doc2PdfHelper {

	@Value("${tmp.dir}")
	private String tmpDir;

	public String save(JsonFile jsonFile) {
		String fileName = jsonFile.getFileName();
		String jsonContent = jsonFile.getFileBytes();
		String docxFilePath = tmpDir + fileName;
		byte[] bytesContent = Base64.getDecoder().decode(jsonContent);
		try (FileOutputStream fos = new FileOutputStream(docxFilePath)) {
			fos.write(bytesContent);
		} catch (IOException e) {
			throw new IllegalStateException("could not write file " + fileName, e);
		}
		return docxFilePath;
	}

	public String load(String pdfFilePath) {
		try {
			Path path = Paths.get(pdfFilePath);
			byte[] bytesContent = Files.readAllBytes(path);
			return Base64.getEncoder().encodeToString(bytesContent);
		} catch (IOException e) {
			throw new IllegalStateException("could not read file " + pdfFilePath, e);
		}
	}
}
