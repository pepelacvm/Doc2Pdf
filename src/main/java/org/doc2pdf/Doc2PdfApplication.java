package org.doc2pdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Doc2PdfApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(Doc2PdfApplication.class, args);
	}

}
