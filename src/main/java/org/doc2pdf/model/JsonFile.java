package org.doc2pdf.model;

import com.fasterxml.jackson.annotation.JsonAlias;

public class JsonFile {
	@JsonAlias({ "FileName" })
	private String fileName;
	@JsonAlias({ "FileBytes" })
	private String fileBytes;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(String fileBytes) {
		this.fileBytes = fileBytes;
	}
}
